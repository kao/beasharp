﻿using System;
using System.Runtime.InteropServices;

namespace Bea
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class REX_Struct
    {
        public byte W_;
        public byte R_;
        public byte X_;
        public byte B_;
        public byte state;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PrefixInfo
    {
        public int Number;
        public int NbUndefined;
        public byte LockPrefix;
        public byte OperandSize;
        public byte AddressSize;
        public byte RepnePrefix;
        public byte RepPrefix;
        public byte FSPrefix;
        public byte SSPrefix;
        public byte GSPrefix;
        public byte ESPrefix;
        public byte CSPrefix;
        public byte DSPrefix;
        public byte BranchTaken;
        public byte BranchNotTaken;
        public REX_Struct REX;
        public Int32 alignment;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class EFLStruct
    {
        public byte OF_;
        public byte SF_;
        public byte ZF_;
        public byte AF_;
        public byte PF_;
        public byte CF_;
        public byte TF_;
        public byte IF_;
        public byte DF_;
        public byte NT_;
        public byte RF_;
        public byte alignment;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class MemoryType
    {
        public Int32 BaseRegister;
        public Int32 IndexRegister;
        public Int32 Scale;
        public Int64 Displacement;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class InstructionType
    {
        public Int32 Category;
        public Int32 Opcode;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
        public string Mnemonic;
        public Int32 BranchType;
        public EFLStruct Flags;
        public UInt64 AddrValue;
        public Int64 Immediat;
        public UInt32 ImplicitModifiedRegs;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class ArgumentType
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string ArgMnemonic;
        public Int32 ArgType;
        public Int32 ArgSize;
        public Int32 ArgPosition;
        public UInt32 AccessMode;
        public MemoryType Memory;
        public UInt32 SegmentReg;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class Disasm
    {
        public IntPtr EIP;
        public UInt64 VirtualAddr;
        public UInt32 SecurityBlock;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string CompleteInstr;
        public UInt32 Archi;
        public UInt64 Options;
        public InstructionType Instruction;
        public ArgumentType Argument1;
        public ArgumentType Argument2;
        public ArgumentType Argument3;
        public PrefixInfo Prefix;
        InternalDatas Reserved_;
    }

    /* reserved structure used for thread-safety */
    /* unusable by customer */
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class InternalDatas
    {
        public UIntPtr EIP_;
        public UInt64 EIP_VA;
        public UIntPtr EIP_REAL;
        public Int32 OriginalOperandSize;
        public Int32 OperandSize;
        public Int32 MemDecoration;
        public Int32 AddressSize;
        public Int32 MOD_;
        public Int32 RM_;
        public Int32 INDEX_;
        public Int32 SCALE_;
        public Int32 BASE_;
        public Int32 MMX_;
        public Int32 SSE_;
        public Int32 CR_;
        public Int32 DR_;
        public Int32 SEG_;
        public Int32 REGOPCODE;
        public UInt32 DECALAGE_EIP;
        public Int32 FORMATNUMBER;
        public Int32 SYNTAX_;
        public UInt64 EndOfBlock;
        public Int32 RelativeAddress;
        public UInt32 Architecture;
        public Int32 ImmediatSize;
        public Int32 NB_PREFIX;
        public Int32 PrefRepe;
        public Int32 PrefRepne;
        public UInt32 SEGMENTREGS;
        public UInt32 SEGMENTFS;
        public Int32 third_arg;
        public Int32 TAB_;
        public Int32 ERROR_OPCODE;
        public REX_Struct REX;
        public Int32 OutOfBlock;
    }
}
